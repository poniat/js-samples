(function($){

    $.properties_search = function(place, options)
    {
        var Search = this;

        // settings
        Search.options = $.extend(true, {
            html: {}
        }, options);

        Search.base$El = $(place);
        Search.id = '#'+Search.base$El.attr('id');


        Search.filters = function (pageNumber) {
            var serialize = Search.base$El.find('form[name="search"]:visible').serialize(),
                form = serialize + '&' + $.param({page: pageNumber, order_by: Search.getOrderBy()}),
                page = (pageNumber >= 1) ? '?page='+pageNumber+'&': '';
            
            // change parameters in search url
            if (typeof (history.pushState) != "undefined")
                history.pushState(null, null, '/search/' + page + form);

            // go to top of the results page
            location.hash = '#properties_results';

            $.ajax({
                type: 'POST',
                url: '/ajax/properties/search.ajax.php',
                data: form,
                dataType: 'json',
                beforeSend: function () {
                    Search.loaderShow();
                },
                success: function (data) {
                    var results = $('#properties'),
                        property = $('.property-block'),
                        property_clone = property.first().clone(), href = '', from = '', price = '',
                        items = data.items;


                    // remove all old properties
                    results.empty();

                    // copy first property html chunk and attache to property result
                    // ( that's only html template - we use that only to populate the property list )
                    results.append(property_clone);

                    // Get the value of the property type you're searching for
                    var propertyTypeId = Search.base$El.find('form[name="search"]:visible').find('select').val(),
                        propertyChangeable = $('body').find('.property-changeable');

                    // Change the property type in the search heading 
                    if (propertyTypeId) {
                        var propertyTypeName = '';
                        switch (propertyTypeId) {
                            case '1':
                                propertyTypeName = 'student';
                                break;
                            case '2':
                                propertyTypeName = 'professional';
                                break;
                            case '3':
                                propertyTypeName = 'commercial';
                                break;
                        }
                        propertyChangeable.html(propertyTypeName);
                    }
                    
                    if (items != null)
                    {
                        var len = items.length,
                            let = '', from = '',
                            admin_edit_link;

                        for (var i = 0; i < len; i++)
                        {
                            //hide  first property box
                            property.first().toggle();
                            // copy property box
                            property_clone = property.first().clone();
                            // make property box visible
                            property_clone.removeAttr('style');
                            // if property is saved in favorite then add favorite css class
                            if (items[i].favorite === true)
                                property_clone.find('.heart-property').addClass('favorite');
                            // attache property id
                            property_clone.attr('data-property-id', items[i].property.id);
                            // admin edit link
                            admin_edit_link = property_clone.find('.hub_editable_item a').attr('href');
                            property_clone.find('.hub_editable_item a').attr('href', admin_edit_link+items[i].property.id);
                            // title
                            property_clone.find('.property-block-details').append('<span class="title">' + items[i].property.title + '</span><br>');
                            // address
                            //property_clone.find('.address').append(items[i].property.address);
                            // area
                            property_clone.find('.property-block-details').append('<span class="city">' + items[i].property.area_id + '</span><br>');
                            // postcode
                            //property_clone.find('.property-block-details').append('<span class="postcode">' + items[i].property.postcode + '</span><br>');

                            // rent
                            if(parseFloat(items[i].property.rent) > 0)
                            {
                                if(items[i].property.type_id == 1) // students
                                    from = 'From';
                                price = items[i].property.rent;
                            }
                            else
                                price = items[i].property.rent_all_inc + ' all inc';

                            property_clone.find('.rent').html(from+' &pound;' + price);
                            // bedrooms
                            property_clone.find('.property-block-button').append(items[i].property.bedrooms);
                            // append price suffix
                            property_clone.find('.suffix').append(Search.getPriceSuffix(items[i].property.type_id));
                            // add property image
                            property_clone.find('.cover').css("background-image", "url('"+items[i].image+"')");
                            // create hyperlink to property details web page
                            href = property_clone.find('a.property-block-button').attr('href');
                            property_clone.find('a.property-block-button').attr('href', href + items[i].property.uri);
                            // add let
                            if(items[i].property.let == 1)
                            {
                                let = $('<span />', {class: 'let', text: 'Let agreed'});
                                property_clone.find('.title:first').after(let);
                            }

                            // remove hide class
                            property_clone.removeClass('hide');
                            // add property
                            results.append(property_clone);
                        }
                    } else {
                        results.append('<h2 class="align-center">No properties found</h2>');
                    }

                    // put number of properties
                    $('#result_count').html(data.count);

                    // added pagination
                    if (typeof data.pagination != 'undefined')
                        $('#pagination').html(data.pagination);

                    $('#map-canvas-wrapper').empty();

                    // create new map data
                    if (typeof data.map != 'undefined' && data.map != false && data.map != null) {
                        $('#map-canvas-wrapper').append('<div id="map-canvas" class="google-map" style="width:100%;height:500px;"></div>');
                        window.site.locations = data.map;

                        gMapInitialize();
                        dropMarkers();
                        $('#map_view, #map-canvas-wrapper').hide();
                        $('#grid_view, #properties').show();
                    }
                    else {
                        $('#map-canvas-wrapper').append('<h2>No property locations available</h2>');
                    }

                    Search.loaderHide();
                },
                error: function () {
                    console.log('Ajax error');
                }
            });
        };

        Search.getPriceSuffix = function (propertyType) {
            if (propertyType) {
                var suffix = '';
                switch (propertyType) {
                    case '1':
                        suffix = 'PPPW';
                        break;
                    case '2':
                        suffix = 'PPPM';
                        break;
                    case '3':
                        suffix = 'PA';
                        break;
                }

                return suffix;
            }

            return false;
        };

        Search.pagination = function (pageNumber) {
            Search.filters(pageNumber);
        };

        Search.getCurrentPage = function () {
            return $(this.id + ' .pagination-list > li.active').children().html();
        };

        Search.getOrderBy = function () {
            return $(this.id + ' select[name="order"]').val();
        };

        Search.loaderShow = function () {
            $('#loader').show();
        };

        Search.loaderHide = function () {
            $('#loader').hide();
        };

        Search.bindEvents = function () {

            Search.base$El.on('click', '.pagination-list a', function (e) {
                e.preventDefault();
                Search.filters($(this).attr('href'));
            }).on('submit', 'form[name="search"]', function (e) {
                e.preventDefault();
                Search.filters(1);
            }).on('change', 'select[name="order"]', function (e) {
                e.preventDefault();
                Search.filters(1);
            });

        };


        Search.bindEvents();
        Search.loaderHide();

    };

    $.fn.properties_search = function (options) {
        return this.each(function() {
            if (undefined == $(this).data('properties_search')) {
                var plugin = new $.properties_search(this, options);
                $(this).data('properties_search', plugin);
            }
        });
    };

    // call search properties plugin
    $('#properties_results').properties_search();



})(jQuery);