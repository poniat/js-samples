/**
 * File uploader (with tooltip)
 * Marcin Galaszewski
 * 19/04/2016
 * ver. 0.1
 */
$(function(){

    $.uploader = function(place, options)
    {
        // initialise basic plugins vars
        var Uploader = this;
        var base = $(place);
        var settings = $.extend(true, {

        }, options);


        var ajaxManager = (function() {
            var requests = [],
                xhr;

            return {
                abort: function(id) {
                    if (xhr) {
                        xhr.abort();
                    }
                },
                addReq: function(opt) {
                    requests.push(opt);
                },
                removeReq: function(opt) {
                    if( $.inArray(opt, requests) > -1 )
                        requests.splice($.inArray(opt, requests), 1);
                },
                run: function() {
                    var self = this,
                        oriSuc,i=0;

                    if( requests.length ) {
                        oriSuc = requests[0].complete;

                        requests[0].complete = function() {
                            if( typeof(oriSuc) === 'function' ) oriSuc();
                            requests.shift();

                            self.run.apply(self, []);
                        };

                        xhr = $.ajax(requests[0]);
                        i++;
                    } else {
                        self.tid = setTimeout(function() {
                            self.run.apply(self, []);
                        }, 100);
                    }
                },
                stop: function() {
                    requests = [];
                    clearTimeout(this.tid);
                }
            };
        }());

        Uploader.progressHandlingFunction = function () {
            return function () {
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) { // Check if upload property exists
                    myXhr.upload.addEventListener('progress', function (event) { // For handling the progress of the upload
                        if(event.lengthComputable){
                            base.find('.progress-bar-wrapper').show();
                            var progress_bar = base.find('.progress-bar'),
                                percent = (event.loaded / event.total) * 100;

                                progress_bar.css({width: percent+'%'});
                                progress_bar.find('.label').html(parseInt(percent * 1)  + '%');
                        }
                    }, false);

                    return myXhr;
                }
            }
        };

        Uploader.sendFile = function(files) {
            var len = files.length;

            for(var i=0; i<len; i++)
            {
                var that = this, uri = Uploader.getData(),
                    formData = new FormData();
                formData.append("file", files[i]);

                ajaxManager.addReq({
                    url: 'resources/link-resource.ajax.php/?'+uri,
                    type: 'POST',
                    dataType: 'json',
                    xhr: Uploader.progressHandlingFunction(),
                    //Ajax events
                    // beforeSend: beforeSendHandler,
                    success: Uploader.completeHandler,
                    // error: errorHandler,
                    // Form data
                    data: formData,
                    mimeType: "multipart/form-data",
                    //Options to tell jQuery not to process data or worry about content-type.
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        };

        Uploader.completeHandler = function(data) {
            if(data.status == 'error')
            {
                var message = base.find('.message');
                message.html(data.message).slideDown();
                setTimeout(function() {
                    message.slideUp();
                }, 6000);
            }
            else
            {
                var $div = Uploader.createFileName(data.message.id, data.message.file_name, data.message.path);
                $div.find('img').css('background', 'transparent');
                base.find(".resources-container").append($div).show();
            }

            var wrapper = base.find('.progress-bar-wrapper'),
                progress_bar = wrapper.find('.progress-bar');

            progress_bar.css({width: '0px'});
            progress_bar.find('.label').html('');
            wrapper.hide();
            return false;
        };

        Uploader.delSymptom = function (id, section, selector)
        {
            if(id)
            {
                $.post("resources/unlink-resource.ajax.php", {id: id, section: section}, function( data ) {

                    selector.fadeOut("medium", function() {
                        $(this).remove();
                        Uploader.reorderResources(section);
                    });

                }, "json");
            }
        };

        Uploader.loadImages = function (arr, key)
        {
            if(arr.length > 0){
                var downloadingImage = $("<img>");
                downloadingImage.attr("src", arr[0].path);
                downloadingImage.load(function(){
                    var img = base.find(".resources-container img:eq("+key+")");
                    img.parent().data('img-id', arr[0].id);
                    img.attr("src", $(this).attr("src"));
                    img.css('background', 'transparent');
                    key++;
                    // remove first image
                    arr.shift();

                    Uploader.loadImages(arr, key)
                });
            }
        };

        Uploader.getImages = function ()
        {
            $(window).load(function() {
                var uri = Uploader.getData();

                $.get( "resources/fetch-resources.ajax.php/?"+uri, function( data ) {
                    for (var i = 0; i < data.length; i++)
                    {
                        var $div = Uploader.createFileName(data[i].id, data[i].file_name, '');
                        base.find('.resources-container').show().append($div);
                    }

                    Uploader.loadImages(data, 0);
                }, 'json');
            });
        };

        Uploader.createFileName = function (file_id, original_file_name, path)
        {
            var $div = $('<li>', {class: 'thumb-wrapper', 'data-resource-id': file_id, id: 'thumb_'+file_id}),
             $img = $("<img />", {src: path, class: 'thumb'}),
             $span = $('<span>', {class: 'delete', html: '&times;'}),
             $file_name = '';

            if(original_file_name)
            {
                $file_name = original_file_name;
                if($file_name.length > 20)
                {
                    $file_name = $file_name.substring(0,20)+'...'
                }
                $file_name = $('<span>', {class: 'file-name tooltip', html: $file_name, title: original_file_name});
                $file_name = $('<br>').add($file_name);
            }

            $div.append($img, $span, $file_name);
            return $div;
        };

        Uploader.getAttributes = function ()
        {
            return eval('(' + base.find('.get-data').data('url') + ')');
        };

        Uploader.getData = function ()
        {
            var attrs = Uploader.getAttributes(),
                uri = '';

            if(attrs) {
                // create uri
                $.each(attrs, function(k, v){
                    uri += k+'='+v+'&'
                });
                uri = uri.slice(0,-1);
            }

            return uri;
        };

        Uploader.getSection = function ()
        {
            var attrs = Uploader.getAttributes();
            return attrs.section;
        };

        Uploader.reorderResources = function ()
        {
            var section = Uploader.getSection(),
                positions = base.find('.resources-container').sortable("toArray");
                $.post("resources/reorder-resources.ajax.php", {positions: positions, section: section});
        };

        Uploader.tooltip = function()
        {
            var xOffset = 4,
                yOffset = 20,
                title;

            base.on('mouseenter', '.tooltip', function(e){
                title = $(this).attr('title');
                this.title = '';
                base.append("<p id='tooltip'>"+ title +"</p>");
                $("#tooltip")
                    .css("top",(e.pageY - xOffset) + "px")
                    .css("left",(e.pageX + yOffset) + "px")
                    .fadeIn("fast");
            }).on('mouseleave', '.tooltip', function(e){
                this.title = title;
                $("#tooltip").remove();
            }).on('mousemove', '.tooltip', function(e){
                $("#tooltip")
                    .css("top",(e.pageY - xOffset) + "px")
                    .css("left",(e.pageX + yOffset) + "px");
            });

        };

        var bindEvents = function ()
        {
            var that = this;
            base.on('change', 'input[name="file"]', function(e){
                e.preventDefault();

                Uploader.sendFile(this.files);
            }).on('click', '.delete', function(){
                var that = $(this),
                    id = that.closest('[data-resource-id], .thumb-wrapper').data('resource-id'),
                    section = Uploader.getSection();
                    $("body").confirm({
                        title: '<i class="picture_delete"></i>Remove Image',
                        message: '<p>Are you sure you want to remove this image?<br /><br />Removing an image will not delete it from the image manager.</p>',
                        buttons: [
                            {
                                text: "Yes",
                                callback: function() {
                                    Uploader.delSymptom(id, section, that.parent());
                                    $("body").confirm("close");
                                }
                            },
                            {
                                text: "No",
                                callback: function() {
                                    $("body").confirm("close");
                                }
                            }
                        ]
                    }).confirm("open");

                return false;
            });

            // drag & drop
            base.find('.drop')
                .on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                })
                .on('dragover dragenter', function (e)
                {
                    $(this).addClass('is-dragover');
                    $(this).removeClass('drop');
                })
                .on('dragleave dragend drop', function (e)
                {
                    $(this).addClass('drop');
                    $(this).removeClass('is-dragover');
                })
                .on('drop', function (e)
                {
                    Uploader.sendFile(e.originalEvent.dataTransfer.files);
                });

        };

        Uploader.getImages();
        Uploader.tooltip();
        bindEvents();
        ajaxManager.run();
    };


    $.fn.uploader = function (options) {
        return this.each(function() {
            if (undefined == $(this).data('uploader')) {
                var plugin = new $.uploader(this, options);
                $(this).data('uploader', plugin);
            }
        });
    };

});